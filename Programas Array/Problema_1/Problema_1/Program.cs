﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numeros = new double[4];
            double suma = 0;
            double media;
            Console.WriteLine("Introduce los 4 numeros");
            for (int c = 0; c < 4; c++)
            {
                Console.Write("Introduzca el valor {0}: ", c + 1);
                numeros[c] = Convert.ToDouble(Console.ReadLine());
                suma += numeros[c];
            }
            media = suma / 4;
            Console.Write("\nLos numeros introducidos fueron: ");
            for (int c = 0; c < 4; c++)
            {
                Console.Write(" ({0}) ", numeros[c]);
            }
            Console.Write("\nY la media aritmetica es: {0}", media);
            Console.ReadKey();

        }
    }
}
